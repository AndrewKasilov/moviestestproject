package com.kasilov.andrew.movies.data.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.kasilov.andrew.movies.data.db.entity.MovieDescription;

import java.util.List;

/**
 * Created by user on 15-Jun-17.
 */
@Dao
public interface MovieDao {

    @Query("SELECT * FROM movie_description")
    List<MovieDescription> getAllMovies();

    @Query("SELECT * FROM movie_description")
    LiveData<List<MovieDescription>> getAll();

    @Insert()
    void saveMovies(List<MovieDescription> movieDescriptions);

    @Query("SELECT * FROM movie_description WHERE i = :movieId")
    LiveData<MovieDescription> getMovie(long movieId);

    @Delete
    void deleteAll(List<MovieDescription> movieDescriptions);

}
