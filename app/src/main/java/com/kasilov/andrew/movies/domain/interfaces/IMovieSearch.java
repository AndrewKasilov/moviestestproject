package com.kasilov.andrew.movies.domain.interfaces;

import com.kasilov.andrew.movies.data.network.responses.AllMoviesResponse;

import io.reactivex.Observer;

/**
 * Created by user on 19-Jul-17.
 */

public interface IMovieSearch {

    void searchMovies(Observer<AllMoviesResponse> completableObserver, String query, int page);
}
