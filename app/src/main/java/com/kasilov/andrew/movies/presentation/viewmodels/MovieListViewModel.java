package com.kasilov.andrew.movies.presentation.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.kasilov.andrew.movies.data.db.AppDataBase;
import com.kasilov.andrew.movies.data.db.DatabaseCreator;
import com.kasilov.andrew.movies.data.db.entity.MovieDescription;
import com.kasilov.andrew.movies.domain.interactors.ClearDBUseCase;
import com.kasilov.andrew.movies.domain.interactors.LoadMoviesUseCase;
import com.kasilov.andrew.movies.presentation.interfaces.IRequestListener;

import java.util.List;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by user on 15-Jun-17.
 */

public class MovieListViewModel extends AndroidViewModel {

    private final String TAG = getClass().getSimpleName();
    private MutableLiveData<IRequestListener.Status> requestStatusLiveData = new MutableLiveData<>();
    private MutableLiveData<Boolean> showSnackBarLiveData = new MutableLiveData<>();

    private LoadMoviesUseCase loadMoviesUseCase;
    private ClearDBUseCase clearDBUseCase;
    private LiveData<List<MovieDescription>> listLiveData;

    public MovieListViewModel(Application application) {
        super(application);
        this.loadMoviesUseCase = new LoadMoviesUseCase();
        this.clearDBUseCase = new ClearDBUseCase();
        AppDataBase dataBase = DatabaseCreator.getInstance().getDatabase();
        updateLiveData(dataBase);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        this.loadMoviesUseCase.dispose();
        this.clearDBUseCase.dispose();
    }

    private void updateLiveData(AppDataBase dataBase) {
        this.listLiveData = dataBase.getMovieDao().getAll();
    }

    public LiveData<List<MovieDescription>> getMovies() {
        return listLiveData;
    }

    public MutableLiveData<IRequestListener.Status> getRequestStatusLiveData() {
        return requestStatusLiveData;
    }

    public MutableLiveData<Boolean> getShowSnackBarLiveData() {
        return showSnackBarLiveData;
    }

    public void loadMovies(int page) {
        this.requestStatusLiveData.setValue(IRequestListener.Status.LOADING);
        this.loadMoviesUseCase.execute(new MoviesLoaderObserver(), LoadMoviesUseCase.Params.Companion.withPage(page));
    }

    public void clearDB() {
        this.clearDBUseCase.execute(new ClearDBObserver(), ClearDBUseCase.Params.Companion.empty());
    }


    private class MoviesLoaderObserver extends DisposableObserver<Boolean> {

        @Override
        public void onNext(Boolean aBoolean) {

        }

        @Override
        public void onError(Throwable e) {
            Log.e(TAG, "onError: ", e);
            requestStatusLiveData.setValue(IRequestListener.Status.ERROR);
            showSnackBarLiveData.setValue(true);
            e.printStackTrace();
        }

        @Override
        public void onComplete() {
            requestStatusLiveData.setValue(IRequestListener.Status.COMPLETED);
        }
    }

    private class ClearDBObserver extends DisposableObserver<Boolean> {

        @Override
        public void onNext(Boolean aBoolean) {

        }

        @Override
        public void onError(Throwable e) {
            Log.e(TAG, "onError: ", e);
            e.printStackTrace();
        }

        @Override
        public void onComplete() {
            Log.d(TAG, "DB cleared");
        }
    }
}
