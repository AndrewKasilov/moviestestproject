package com.kasilov.andrew.movies.data.network.endpoints;

import com.kasilov.andrew.movies.data.network.responses.AllMoviesResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by user on 15-Jun-17.
 */

public interface MoviesAPI {

    @GET("discover/movie")
    Observable<AllMoviesResponse> getMoviesByPage(@Query("page") int page);

    @GET("search/movie")
    Observable<AllMoviesResponse> searchMovie(@Query("query") String query, @Query("page") int page);
}
