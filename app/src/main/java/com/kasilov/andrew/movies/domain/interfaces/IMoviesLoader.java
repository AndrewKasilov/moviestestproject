package com.kasilov.andrew.movies.domain.interfaces;

import io.reactivex.CompletableObserver;

/**
 * Created by user on 13-Jun-17.
 */

public interface IMoviesLoader {

    void loadMovies(CompletableObserver completableObserver, int page);
}
