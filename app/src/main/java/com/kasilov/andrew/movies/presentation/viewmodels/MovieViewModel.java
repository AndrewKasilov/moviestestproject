package com.kasilov.andrew.movies.presentation.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.kasilov.andrew.movies.data.db.AppDataBase;
import com.kasilov.andrew.movies.data.db.DatabaseCreator;
import com.kasilov.andrew.movies.data.db.entity.MovieDescription;

/**
 * Created by user on 18-Jul-17.
 */

public class MovieViewModel extends AndroidViewModel {

    private LiveData<MovieDescription> movieLiveData;

    public MovieViewModel(Application application, long movieId) {
        super(application);

        AppDataBase dataBase = DatabaseCreator.getInstance().getDatabase();
        updateLiveData(dataBase, movieId);
    }

    private void updateLiveData(AppDataBase dataBase, long movieId) {
        movieLiveData = dataBase.getMovieDao().getMovie(movieId);
    }

    public LiveData<MovieDescription> getMovieLiveData() {
        return movieLiveData;
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        private final Application application;
        private final long movieId;

        public Factory(Application application, long movieId) {
            this.application = application;
            this.movieId = movieId;
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            return (T) new MovieViewModel(application, movieId);
        }
    }
}
