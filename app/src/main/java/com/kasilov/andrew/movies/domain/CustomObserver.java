package com.kasilov.andrew.movies.domain;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by user on 07-Jul-17.
 */

public class CustomObserver<T> extends DisposableObserver<T> {

    @Override
    public void onNext(T t) {

    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onComplete() {

    }
}
