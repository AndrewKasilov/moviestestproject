package com.kasilov.andrew.movies.presentation.adapters;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.kasilov.andrew.movies.R;
import com.kasilov.andrew.movies.data.db.entity.MovieDescription;
import com.kasilov.andrew.movies.databinding.FooterItemBinding;
import com.kasilov.andrew.movies.databinding.MovieItemBinding;
import com.kasilov.andrew.movies.presentation.interfaces.IListScrollListener;
import com.kasilov.andrew.movies.presentation.interfaces.IMovieClickCallback;
import com.kasilov.andrew.movies.presentation.interfaces.IRequestListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by user on 15-Jun-17.
 */

public class MovieListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int FOOTER_VIEW_TYPE = 4564;
    private static final int MOVIE_VIEW_TYPE = 7897;
    private final String TAG = getClass().getSimpleName();
    private final IMovieClickCallback iMovieClickCallback;
    private final IListScrollListener iListScrollListener;
    private List<MovieDescription> data;
    private IRequestListener.Status requestStatus = IRequestListener.Status.COMPLETED;
    private boolean loading;

    public MovieListAdapter(@Nullable IMovieClickCallback iMovieClickCallback, IListScrollListener iListScrollListener) {
        this.iMovieClickCallback = iMovieClickCallback;
        this.iListScrollListener = iListScrollListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == MOVIE_VIEW_TYPE) {
            return getMovieViewHolder(viewGroup);
        } else {
            return getFooterViewHolder(viewGroup);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position == data.size() ? FOOTER_VIEW_TYPE : MOVIE_VIEW_TYPE;
    }

    private FooterViewHolder getFooterViewHolder(ViewGroup viewGroup) {
        FooterItemBinding footerItemBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.footer_item, viewGroup, false);
        return new FooterViewHolder(footerItemBinding);
    }

    @NonNull
    private MovieViewHolder getMovieViewHolder(ViewGroup viewGroup) {
        MovieItemBinding movieItemBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.movie_item, viewGroup, false);
        movieItemBinding.setCallback(iMovieClickCallback);
        return new MovieViewHolder(movieItemBinding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MovieViewHolder) {
            onBindViewHolder((MovieViewHolder) holder, position);
        } else if (holder instanceof FooterViewHolder) {
            onBindViewHolder((FooterViewHolder) holder);
        }
    }

    private void onBindViewHolder(MovieViewHolder movieViewHolder, int position) {
        movieViewHolder.itemBinding.setMovie(data.get(position));
        movieViewHolder.itemBinding.executePendingBindings();
        if (position == data.size() - 5) {
            this.iListScrollListener.onListEndReached();
        }
    }

    private void onBindViewHolder(FooterViewHolder footerViewHolder) {
        footerViewHolder.itemBinding.setStatus(requestStatus);
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size() + 1;
    }

    public void setDataList(List<MovieDescription> movieDescriptions) {
        if (data == null) {
            data = new ArrayList<>(movieDescriptions);
            notifyItemRangeInserted(0, movieDescriptions.size());
        } else {
            DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return data.size();
                }

                @Override
                public int getNewListSize() {
                    return movieDescriptions.size();
                }

                @Override
                public boolean areItemsTheSame(int i, int i1) {
                    return data.get(i).getId() == movieDescriptions.get(i1).getId();
                }

                @Override
                public boolean areContentsTheSame(int i, int i1) {
                    return data.get(i).getId() == movieDescriptions.get(i1).getId()
                            && Objects.equals(data.get(i).getTitle(), movieDescriptions.get(i1).getTitle());
                }
            }, false);
            this.data = new ArrayList<>(movieDescriptions);
            diffResult.dispatchUpdatesTo(this);
        }
    }

    public void setRequestStatus(IRequestListener.Status status) {
        this.requestStatus = status;
    }

    class MovieViewHolder extends RecyclerView.ViewHolder {

        private final MovieItemBinding itemBinding;

        MovieViewHolder(MovieItemBinding itemView) {
            super(itemView.getRoot());
            this.itemBinding = itemView;
        }
    }

    private class FooterViewHolder extends RecyclerView.ViewHolder {

        private final FooterItemBinding itemBinding;

        FooterViewHolder(FooterItemBinding itemBinding) {
            super(itemBinding.getRoot());
            this.itemBinding = itemBinding;

        }
    }
}
