package com.kasilov.andrew.movies.presentation.view.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.kasilov.andrew.movies.R;
import com.kasilov.andrew.movies.databinding.ActivityMainBinding;
import com.kasilov.andrew.movies.presentation.adapters.MainMenuFragmentsAdapter;
import com.kasilov.andrew.movies.presentation.interfaces.ISnackBar;
import com.kasilov.andrew.movies.presentation.view.fragments.MoviesListFragment;
import com.kasilov.andrew.movies.presentation.view.fragments.SearchMovieFragment;
import com.kasilov.andrew.movies.presentation.viewmodels.MovieListViewModel;

public class MainActivity extends AppCompatActivity implements ISnackBar {

    private final String TAG = getClass().getSimpleName();
    private Snackbar snackBar;
    private MainMenuFragmentsAdapter mainMenuFragmentsAdapter;
    private MovieListViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate: ");
        initUI();
        initViewModel();
    }

    private void initViewModel() {
        viewModel = ViewModelProviders.of(this).get(MovieListViewModel.class);
    }

    private void initUI() {
        Log.d(TAG, "initUI: ");
        ActivityMainBinding activityMainBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_main, null, false);
        setContentView(activityMainBinding.getRoot());

        setupFragments(activityMainBinding);
        setSupportActionBar(activityMainBinding.toolbar);
        snackBar = Snackbar.make(findViewById(R.id.constraint_layout_main), "", Snackbar.LENGTH_LONG);
    }

    private void setupFragments(ActivityMainBinding activityMainBinding) {
        mainMenuFragmentsAdapter = new MainMenuFragmentsAdapter(getSupportFragmentManager());
        mainMenuFragmentsAdapter.addFragment(new MoviesListFragment(), "All");
        mainMenuFragmentsAdapter.addFragment(new SearchMovieFragment(), "Search");
        activityMainBinding.viewPagerMainActivity.setAdapter(mainMenuFragmentsAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_clear_db:
                viewModel.clearDB();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showSnackBar(String s) {
        snackBar.setText(s);
        snackBar.show();
    }

}
