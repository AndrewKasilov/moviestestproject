package com.kasilov.andrew.movies.domain.interfaces;

/**
 * Created by user on 24-Jul-17.
 */

public interface IDBCleaner {

    void deleteDB();
}
