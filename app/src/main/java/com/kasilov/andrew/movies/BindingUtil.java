package com.kasilov.andrew.movies;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.kasilov.andrew.movies.data.network.RetrofitNetManager;

/**
 * Created by user on 05-Jul-17.
 */

public class BindingUtil {

    private static final String SIZE_92 = "w92/";
    private static final String POSTER_SIZE_154 = "w154/";
    private static final String SIZE_780 = "w780/";
    private static final String TAG = "BindingUtil";

    @BindingAdapter("app:imageUrl")
    public static void imageUrl(ImageView v, String url) {
        if (url == null) return;
        Glide.with(v.getContext())
                .load(RetrofitNetManager.BASE_IMAGE_URL + SIZE_780 + url)
                .into(v);

    }

    @BindingAdapter("app:loadSmallImage")
    public static void loadSmallImage(ImageView v, String url) {
        if (url == null) return;
        Glide.with(v.getContext())
                .load(RetrofitNetManager.BASE_IMAGE_URL + SIZE_92 + url)
                .into(v);

    }
}
