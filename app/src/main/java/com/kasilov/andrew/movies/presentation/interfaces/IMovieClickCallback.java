package com.kasilov.andrew.movies.presentation.interfaces;

import com.kasilov.andrew.movies.data.db.entity.MovieDescription;

/**
 * Created by user on 15-Jun-17.
 */

public interface IMovieClickCallback {

    void onMovieClicked(MovieDescription movieDescription);
}
