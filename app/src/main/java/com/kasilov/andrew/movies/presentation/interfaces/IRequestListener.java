package com.kasilov.andrew.movies.presentation.interfaces;

/**
 * Created by user on 06-Jul-17.
 */

public interface IRequestListener {
    void onRequestLoading();

    void onRequestComplete();

    void onRequestError();

    enum Status {
        LOADING,
        COMPLETED,
        ERROR
    }
}
