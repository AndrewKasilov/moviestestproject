package com.kasilov.andrew.movies.data.network.responses;

/**
 * Created by user on 15-Jun-17.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kasilov.andrew.movies.data.db.entity.MovieDescription;

import java.io.Serializable;
import java.util.List;

public class AllMoviesResponse implements Serializable {

    private final static long serialVersionUID = -1684740586006570026L;
    @SerializedName("page")
    @Expose
    private long page;
    @SerializedName("total_results")
    @Expose
    private long totalResults;
    @SerializedName("total_pages")
    @Expose
    private long totalPages;
    @SerializedName("results")
    @Expose
    private List<MovieDescription> movieDescriptions = null;

    public long getPage() {
        return page;
    }

    public void setPage(long page) {
        this.page = page;
    }

    public long getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(long totalResults) {
        this.totalResults = totalResults;
    }

    public long getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(long totalPages) {
        this.totalPages = totalPages;
    }

    public List<MovieDescription> getMovieDescriptions() {
        return movieDescriptions;
    }

    public void setMovieDescriptions(List<MovieDescription> movieDescriptions) {
        this.movieDescriptions = movieDescriptions;
    }
}
