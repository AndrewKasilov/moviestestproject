package com.kasilov.andrew.movies.presentation.view.fragments

import android.arch.lifecycle.LifecycleFragment
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.widget.RxTextView
import com.kasilov.andrew.movies.R
import com.kasilov.andrew.movies.data.db.entity.MovieDescription
import com.kasilov.andrew.movies.databinding.SearchMovieFragmentBinding
import com.kasilov.andrew.movies.domain.UseCaseManager
import com.kasilov.andrew.movies.domain.interfaces.IMovieSearch
import com.kasilov.andrew.movies.presentation.adapters.MovieListAdapter
import com.kasilov.andrew.movies.presentation.interfaces.IListScrollListener
import com.kasilov.andrew.movies.presentation.interfaces.IMovieClickCallback
import com.kasilov.andrew.movies.presentation.view.activities.SelectedMovieActivity
import com.kasilov.andrew.movies.presentation.viewmodels.SearchMovieViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by user on 17-Jul-17.
 */

class SearchMovieFragment : LifecycleFragment(), IMovieClickCallback, IListScrollListener {

    private val TAG: String? = SearchMovieFragment::class.java.simpleName

    private var iMovieSearch: IMovieSearch? = null
    private var binding: SearchMovieFragmentBinding? = null
    private var adapter: MovieListAdapter? = null
    private var viewModel: SearchMovieViewModel? = null
    private var etSearchDisposable: Disposable? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        iMovieSearch = UseCaseManager.getInstance()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.search_movie_fragment, container, false)
        adapter = MovieListAdapter(this, this)
        binding?.rvMovieList?.adapter = adapter
        return binding?.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SearchMovieViewModel::class.java)
        subscribeUI(viewModel)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!etSearchDisposable?.isDisposed!!) etSearchDisposable?.dispose()
    }

    private fun subscribeUI(viewModel: SearchMovieViewModel?) {
        viewModel?.requestStatusLiveData?.observe(this, Observer { status -> adapter?.setRequestStatus(status) })
        viewModel?.searchResult?.observe(this, Observer { movieDescriptions -> if (movieDescriptions != null) adapter?.setDataList(movieDescriptions) })


        etSearchDisposable = RxTextView.textChanges(binding?.etSearch!!).skipInitialValue()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ query: CharSequence -> viewModel?.search(query.toString()) })
    }

    override fun onMovieClicked(movieDescription: MovieDescription?) {
        SelectedMovieActivity.start(this.activity, movieDescription)
    }

    override fun onListEndReached() {
        viewModel?.onListEndReached()
    }
}
