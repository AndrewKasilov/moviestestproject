package com.kasilov.andrew.movies.data.db.converters;

import android.arch.persistence.room.TypeConverter;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 13-Jun-17.
 */

public class Converter {

    @TypeConverter
    public String toString(List<Long> list) {
        JSONArray jsonArray = new JSONArray(list);
        return jsonArray.toString();
    }

    @TypeConverter
    public List<Long> fromString(String s) {
        JSONArray jsonArray;
        List<Long> longList = new ArrayList<>();
        try {
            jsonArray = new JSONArray(s);
            for (int i = 0; i < jsonArray.length(); i++) {
                longList.add(jsonArray.getLong(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return longList;
    }

}
