package com.kasilov.andrew.movies.presentation.interfaces;

/**
 * Created by user on 13-Jul-17.
 */

public interface ISnackBar {

    void showSnackBar(String s);

}
