package com.kasilov.andrew.movies.presentation.view.activities;

import android.app.Activity;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.kasilov.andrew.movies.R;
import com.kasilov.andrew.movies.data.db.entity.MovieDescription;
import com.kasilov.andrew.movies.databinding.ActivitySelectedMovieBinding;

public class SelectedMovieActivity extends AppCompatActivity implements LifecycleRegistryOwner {

    private static final String MOVIE_ID = "MOVIE_ID";
    private static final String MOVIE_DESCRIPTION = "MOVIE_DESCRIPTION";
    private LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    private ActivitySelectedMovieBinding binding;

    public static void start(Activity activity, MovieDescription movieDescription) {
        Intent intent = new Intent(activity, SelectedMovieActivity.class);
        intent.putExtra(MOVIE_DESCRIPTION, movieDescription);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_movie);

        if (getIntent() != null) {
            initUI((MovieDescription) getIntent().getSerializableExtra(MOVIE_DESCRIPTION));
        }
    }

//    private void initViewModel(long movieId) {
//        MovieViewModel.Factory factory = new MovieViewModel.Factory(getApplication(), movieId);
//        MovieViewModel movieViewModel = ViewModelProviders.of(this, factory).get(MovieViewModel.class);
//        subscribeUI(movieViewModel);
//    }

//    private void subscribeUI(MovieViewModel movieViewModel) {
//        movieViewModel.getMovieLiveData().observe(this, movieDescription -> {
//            binding.setMovie(movieDescription);
//            binding.toolbar.setTitle(movieDescription != null ? movieDescription.getTitle() : "");
//        });
//    }

    private void initUI(MovieDescription movieDescription) {
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_selected_movie, null, false);
        binding.setMovie(movieDescription);
        binding.toolbar.setTitle(movieDescription != null ? movieDescription.getTitle() : "");
        setContentView(binding.getRoot());
        setSupportActionBar(binding.toolbar);
    }

    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }

}
