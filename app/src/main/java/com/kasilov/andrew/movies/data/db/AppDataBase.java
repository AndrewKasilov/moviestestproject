package com.kasilov.andrew.movies.data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.kasilov.andrew.movies.data.db.converters.Converter;
import com.kasilov.andrew.movies.data.db.dao.MovieDao;
import com.kasilov.andrew.movies.data.db.entity.MovieDescription;


/**
 * Created by user on 13-Jun-17.
 */

@Database(entities = {MovieDescription.class}, version = 1)
@TypeConverters({Converter.class})
public abstract class AppDataBase extends RoomDatabase {

    public abstract MovieDao getMovieDao();
}
