package com.kasilov.andrew.movies.domain;

import android.util.Log;

import com.kasilov.andrew.movies.data.db.AppDataBase;
import com.kasilov.andrew.movies.data.db.DatabaseCreator;
import com.kasilov.andrew.movies.data.db.entity.MovieDescription;
import com.kasilov.andrew.movies.data.network.RetrofitNetManager;
import com.kasilov.andrew.movies.data.network.responses.AllMoviesResponse;
import com.kasilov.andrew.movies.domain.interfaces.IDBCleaner;
import com.kasilov.andrew.movies.domain.interfaces.IMovieSearch;
import com.kasilov.andrew.movies.domain.interfaces.IMoviesLoader;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by user on 13-Jun-17.
 */

public class UseCaseManager implements IMoviesLoader, IMovieSearch, IDBCleaner {

    private static final Object LOCK = new Object();
    private static UseCaseManager sInstance;
    private final String TAG = getClass().getSimpleName();
    private AppDataBase appDataBase;

    private UseCaseManager() {
        DatabaseCreator.getInstance().isDatabaseCreated().observeForever(aBoolean -> {
                    if (aBoolean != null && aBoolean) {
                        appDataBase = DatabaseCreator.getInstance().getDatabase();
                    }
                }
        );
    }

    public synchronized static UseCaseManager getInstance() {
        if (sInstance == null) {
            synchronized (LOCK) {
                if (sInstance == null) {
                    sInstance = new UseCaseManager();
                }
            }
        }
        return sInstance;
    }

    @Override
    public void loadMovies(CompletableObserver completableObserver, int page) {
        Log.d(TAG, "loadMovies: " + page);
        Completable.create(emitter -> RetrofitNetManager.getInstance().getMoviesAPI().getMoviesByPage(page)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(new DisposableObserver<AllMoviesResponse>() {
                    @Override
                    public void onNext(AllMoviesResponse allMoviesResponse) {
                        for (MovieDescription movieDescription : allMoviesResponse.getMovieDescriptions()) {
                            movieDescription.setPage(page);
                        }
                        appDataBase.getMovieDao().saveMovies(allMoviesResponse.getMovieDescriptions());
                    }

                    @Override
                    public void onError(Throwable e) {
                        emitter.onError(e);
                    }

                    @Override
                    public void onComplete() {
                        emitter.onComplete();
                    }
                }))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(completableObserver);
    }

    @Override
    public void searchMovies(Observer<AllMoviesResponse> disposableObserver, String query, int page) {
        RetrofitNetManager.getInstance().getMoviesAPI().searchMovie(query, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<AllMoviesResponse>() {

                    @Override
                    public void onNext(AllMoviesResponse allMoviesResponse) {
                        disposableObserver.onNext(allMoviesResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        disposableObserver.onError(e);
                    }

                    @Override
                    public void onComplete() {
                        disposableObserver.onComplete();
                    }
                });
    }

    @Override
    public void deleteDB() {
        appDataBase.getMovieDao().deleteAll(appDataBase.getMovieDao().getAllMovies());
    }
}
