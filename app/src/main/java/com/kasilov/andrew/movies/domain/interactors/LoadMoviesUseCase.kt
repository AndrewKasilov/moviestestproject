package com.kasilov.andrew.movies.domain.interactors

import com.kasilov.andrew.movies.data.network.RetrofitNetManager
import com.kasilov.andrew.movies.data.network.responses.AllMoviesResponse
import io.reactivex.Observable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

/**
 * Created by user on 25-Jul-17.
 */
class LoadMoviesUseCase : UseCase<Boolean, LoadMoviesUseCase.Params>() {


    override fun buildObservable(params: Params): Observable<Boolean> {
        return Observable.create<Boolean> { emitter ->
            RetrofitNetManager.getInstance().moviesAPI.getMoviesByPage(params.page)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe(object : DisposableObserver<AllMoviesResponse>() {
                        override fun onNext(allMoviesResponse: AllMoviesResponse) {
                            for (movieDescription in allMoviesResponse.movieDescriptions) {
                                movieDescription.page = params.page.toLong()
                            }
                            appDB?.movieDao?.saveMovies(allMoviesResponse.movieDescriptions)
                        }

                        override fun onError(e: Throwable) {
                            emitter.onError(e)
                            dispose()
                        }

                        override fun onComplete() {
                            emitter.onComplete()
                            dispose()
                        }
                    })
        }
    }


    class Params private constructor(internal val page: Int) {
        companion object {

            fun withPage(page: Int): Params {
                return Params(page)
            }
        }

    }
}