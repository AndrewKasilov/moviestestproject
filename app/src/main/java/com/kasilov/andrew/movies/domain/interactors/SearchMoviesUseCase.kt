package com.kasilov.andrew.movies.domain.interactors

import com.kasilov.andrew.movies.data.network.RetrofitNetManager
import com.kasilov.andrew.movies.data.network.responses.AllMoviesResponse
import io.reactivex.Observable

/**
 * Created by user on 25-Jul-17.
 */
class SearchMoviesUseCase : UseCase<AllMoviesResponse, SearchMoviesUseCase.Params>() {

    override fun buildObservable(params: Params): Observable<AllMoviesResponse> {
        return RetrofitNetManager.getInstance().moviesAPI.searchMovie(params.query, params.page)
    }

    class Params private constructor(val query: String, val page: Int) {

        companion object {
            fun withParams(query: String, page: Int): Params {
                return Params(query, page)
            }
        }
    }
}