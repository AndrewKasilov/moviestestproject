package com.kasilov.andrew.movies.presentation.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.kasilov.andrew.movies.data.db.entity.MovieDescription
import com.kasilov.andrew.movies.data.network.responses.AllMoviesResponse
import com.kasilov.andrew.movies.domain.interactors.SearchMoviesUseCase
import com.kasilov.andrew.movies.presentation.interfaces.IListScrollListener
import com.kasilov.andrew.movies.presentation.interfaces.IRequestListener
import io.reactivex.observers.DisposableObserver
import java.util.*

/**
 * Created by user on 20-Jul-17.
 */

class SearchMovieViewModel(application: Application) : AndroidViewModel(application), IListScrollListener {
    private val TAG: String? = SearchMovieViewModel::class.java.simpleName

    internal var searchResult: MutableLiveData<MutableList<MovieDescription>>? = MutableLiveData()
    private var observer: DisposableObserver<AllMoviesResponse> = MoviesLoaderObserver()
    internal val requestStatusLiveData: MutableLiveData<IRequestListener.Status>? = MutableLiveData()
    private val showSnackBarLiveData: MutableLiveData<Boolean>? = MutableLiveData()
    private var page: Int = 1
    private var query: String? = ""


    private var useCase: SearchMoviesUseCase = SearchMoviesUseCase()

    fun search(query: String) {
        this.searchResult?.value?.clear()
        this.requestStatusLiveData?.value = IRequestListener.Status.LOADING
        this.useCase.execute(MoviesLoaderObserver(), SearchMoviesUseCase.Params.withParams(query, page))
        this.query = query
        this.page = 1
    }

    override fun onListEndReached() {
        this.requestStatusLiveData?.value = IRequestListener.Status.LOADING
        this.useCase.execute(MoviesLoaderObserver(), SearchMoviesUseCase.Params.withParams(query!!, ++page))
    }

    override fun onCleared() {
        super.onCleared()
        this.useCase.dispose()
    }

    private inner class MoviesLoaderObserver : DisposableObserver<AllMoviesResponse>() {


        override fun onNext(t: AllMoviesResponse) {
            if (searchResult?.value == null) {
                searchResult?.value = t.movieDescriptions
            } else if (t.movieDescriptions.size == 0) {
                searchResult?.value = Collections.emptyList()
            } else {
                var tempList: MutableList<MovieDescription> = mutableListOf()
                tempList.addAll(searchResult?.value!!)
                tempList.addAll(t.movieDescriptions)
                searchResult?.value = tempList
            }
        }

        override fun onError(e: Throwable) {
            requestStatusLiveData?.value = IRequestListener.Status.ERROR
            showSnackBarLiveData?.value = true
            searchResult?.value = Collections.emptyList()
            e.printStackTrace()
        }

        override fun onComplete() {
            requestStatusLiveData?.value = IRequestListener.Status.COMPLETED
        }
    }

}
