package com.kasilov.andrew.movies.domain.interactors

import io.reactivex.Observable
import io.reactivex.ObservableEmitter

/**
 * Created by user on 25-Jul-17.
 */
class ClearDBUseCase : UseCase<Boolean, ClearDBUseCase.Params>() {

    override fun buildObservable(params: Params): Observable<Boolean> {
        return Observable.create { emitter: ObservableEmitter<Boolean> ->
            run {
                appDB?.movieDao?.deleteAll(appDB?.movieDao?.allMovies)
                emitter.onComplete()
            }
        }
    }


    class Params {
        companion object {
            fun empty(): Params {
                return Params()
            }
        }
    }
}