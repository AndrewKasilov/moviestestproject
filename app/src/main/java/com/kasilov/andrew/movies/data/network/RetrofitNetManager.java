package com.kasilov.andrew.movies.data.network;

import android.util.Log;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.kasilov.andrew.movies.data.network.endpoints.MoviesAPI;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by user on 30-May-17.
 */

public class RetrofitNetManager {

    public static final String BASE_IMAGE_URL = "http://image.tmdb.org/t/p/";
    private static final String BASE_DATA_URL = "https://api.themoviedb.org/3/";
    private static final String API_KEY = "ce6665100233d0d9a04866fc282739ce";
    private static RetrofitNetManager instance;
    private final String TAG = getClass().getSimpleName();
    private Retrofit retrofit;

    private RetrofitNetManager() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_DATA_URL)
                .client(getHttpClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static RetrofitNetManager getInstance() {
        if (instance == null) {
            instance = new RetrofitNetManager();
        }
        return instance;
    }

    private OkHttpClient getHttpClient() {
        OkHttpClient.Builder httpClient =
                new OkHttpClient.Builder();
        httpClient.addInterceptor(chain -> {
            Request original = chain.request();
            HttpUrl originalHttpUrl = original.url();

            HttpUrl url = originalHttpUrl.newBuilder()
                    .addQueryParameter("api_key", API_KEY)
                    .build();

            // Request customization: add request headers
            Request.Builder requestBuilder = original.newBuilder()
                    .url(url);

            Request request = requestBuilder.build();
            Log.d(TAG, "Request: " + request.toString());
            return chain.proceed(request);
        });
        return httpClient.build();
    }

    public MoviesAPI getMoviesAPI() {
        return retrofit.create(MoviesAPI.class);
    }

}
