package com.kasilov.andrew.movies.presentation.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 17-Jul-17.
 */

public class MainMenuFragmentsAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> fragmentList;
    private List<String> tabsTitleList;

    public MainMenuFragmentsAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabsTitleList.get(position);
    }

    @Override
    public int getCount() {
        return 2;
    }

    public void addFragment(Fragment fragment, String title) {
        if (fragmentList == null) {
            fragmentList = new ArrayList<>();
        }
        if (tabsTitleList == null) {
            tabsTitleList = new ArrayList<>();
        }

        tabsTitleList.add(title);
        fragmentList.add(fragment);
    }
}
