package com.kasilov.andrew.movies.domain.interactors

import com.kasilov.andrew.movies.data.db.AppDataBase
import com.kasilov.andrew.movies.data.db.DatabaseCreator
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

/**
 * Created by user on 25-Jul-17.
 */
abstract class UseCase<T, in Params> {

    internal var appDB: AppDataBase? = DatabaseCreator.getInstance().database
    private var compositeDisposable: CompositeDisposable? = CompositeDisposable()

    abstract internal fun buildObservable(params: Params): Observable<T>

    fun execute(observer: DisposableObserver<T>, params: Params) {
        val observable = buildObservable(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        addDisposable(observable.subscribeWith(observer))
    }

    private fun addDisposable(disposableObserver: Disposable) {
        compositeDisposable?.add(disposableObserver)
    }

    fun dispose() {
        compositeDisposable?.dispose()
    }
}