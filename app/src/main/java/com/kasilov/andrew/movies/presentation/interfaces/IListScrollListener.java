package com.kasilov.andrew.movies.presentation.interfaces;

/**
 * Created by user on 15-Jun-17.
 */

public interface IListScrollListener {
    void onListEndReached();
}
