package com.kasilov.andrew.movies.presentation.view.activities;

import android.arch.lifecycle.LifecycleActivity;
import android.content.Intent;
import android.os.Bundle;

import com.kasilov.andrew.movies.R;
import com.kasilov.andrew.movies.data.db.DatabaseCreator;

public class SplashScreen extends LifecycleActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        DatabaseCreator.getInstance().isDatabaseCreated().observe(this, aBoolean -> {
            if (aBoolean != null && aBoolean) {
                startActivity(new Intent(this, MainActivity.class));
                finish();
            }
        });
    }
}
