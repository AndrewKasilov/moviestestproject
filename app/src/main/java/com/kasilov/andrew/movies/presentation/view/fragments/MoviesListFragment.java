package com.kasilov.andrew.movies.presentation.view.fragments;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kasilov.andrew.movies.R;
import com.kasilov.andrew.movies.data.db.entity.MovieDescription;
import com.kasilov.andrew.movies.databinding.MovieListFragmentBinding;
import com.kasilov.andrew.movies.presentation.adapters.MovieListAdapter;
import com.kasilov.andrew.movies.presentation.interfaces.IListScrollListener;
import com.kasilov.andrew.movies.presentation.interfaces.IMovieClickCallback;
import com.kasilov.andrew.movies.presentation.interfaces.ISnackBar;
import com.kasilov.andrew.movies.presentation.view.activities.MainActivity;
import com.kasilov.andrew.movies.presentation.view.activities.SelectedMovieActivity;
import com.kasilov.andrew.movies.presentation.viewmodels.MovieListViewModel;

/**
 * Created by user on 15-Jun-17.
 */

public class MoviesListFragment extends LifecycleFragment implements IMovieClickCallback, IListScrollListener {

    private final String TAG = getClass().getSimpleName();
    private MovieListFragmentBinding movieListFragmentBinding;
    private MovieListViewModel movieListViewModel;
    private MovieListAdapter movieListAdapter;
    private int page = 1;
    private ISnackBar iSnackBar;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            iSnackBar = (ISnackBar) context;
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        movieListFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.movie_list_fragment, container, false);
        movieListAdapter = new MovieListAdapter(this, this);
        movieListFragmentBinding.rvMovieList.setAdapter(movieListAdapter);
        return movieListFragmentBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "onActivityCreated: ");
        movieListViewModel = ViewModelProviders.of(getActivity()).get(MovieListViewModel.class);
        subscribeUi(movieListViewModel);
    }

    private void subscribeUi(MovieListViewModel movieListViewModel) {

        movieListViewModel.getMovies().observe(this, movieDescriptions -> {
            if (movieDescriptions != null && !movieDescriptions.isEmpty()) {
                movieListAdapter.setDataList(movieDescriptions);
                page = (int) movieDescriptions.get(movieDescriptions.size() - 1).getPage();
            } else {
                movieListViewModel.loadMovies(1);
            }
        });

        movieListViewModel.getRequestStatusLiveData().observe(this, status -> movieListAdapter.setRequestStatus(status));

        movieListViewModel.getShowSnackBarLiveData().observe(this, aBoolean -> {
            if (aBoolean != null && aBoolean) iSnackBar.showSnackBar("Failed to load. Retry?");
        });
    }

    @Override
    public void onMovieClicked(MovieDescription movieDescription) {
        SelectedMovieActivity.start(getActivity(), movieDescription);
    }

    @Override
    public void onListEndReached() {
        movieListViewModel.loadMovies(++page);
    }

}
