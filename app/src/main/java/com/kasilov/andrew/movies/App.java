package com.kasilov.andrew.movies;

import android.app.Application;

import com.kasilov.andrew.movies.data.db.DatabaseCreator;

/**
 * Created by user on 13-Jun-17.
 */

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        DatabaseCreator.getInstance().createDb(this);
    }
}
